//
//  Service.swift
//  weather_sberbank_task
//
//  Created by Иван Рыжухин on 23.06.2020.
//  Copyright © 2020 Ivan Ryzhukhin. All rights reserved.
//

import Foundation
import Alamofire

final class Service {

    enum Constants {
        static let apiKey = "302b5b3153622f3d6f42aa61de61076a"
        static let baseURL = URL(string: "http://api.openweathermap.org/data/2.5/weather")!
    }

    private let sessionManager = Alamofire.Session()
    private let decoder = JSONDecoder()

    private func isSuccessResponse(_ response: HTTPURLResponse?) -> Bool {
        guard let response = response else {
            return false
        }
        let code = response.statusCode
        if  [200, 201, 202, 204].contains(code) {
            return true
        }
        return false

    }

    func getWeather(city:String, completion: ((Bool, String?, WeatherMap?) -> ())? = nil){


        sessionManager.request(Constants.baseURL, method: .get, parameters: ["q": city, "appid": Constants.apiKey, "units": "Metric"]).responseData { [weak self] response in

            guard let strongSelf = self else {
                return
            }

            guard let data  = response.data else {
                completion?(false, "Нет данных", nil)
                return
            }

            guard strongSelf.isSuccessResponse(response.response),  let dataSource = try? JSONDecoder().decode(WeatherMap.self, from: data) else {

                let error =  try? JSONDecoder().decode(Error.self, from: data)
                completion?(false, error?.message, nil)
                return
            }

            completion?(true, nil, dataSource)

            return
        }
    }
}


struct  Error: Codable {
    let cod: String
    let message: String
}
