//
//  Weather.swift
//  weather_sberbank_task
//
//  Created by Иван Рыжухин on 23.06.2020.
//  Copyright © 2020 Ivan Ryzhukhin. All rights reserved.
//

import Foundation

struct Weather: Codable {
    let id: Int
    let description: String?
}

struct Temperature: Codable {
    let temp: Double?
}


struct WeatherMap: Codable {
    var id:Int
    var name: String?
    var weather: [Weather]?
    var main: Temperature?
}
