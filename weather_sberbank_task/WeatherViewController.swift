//
//  WeatherViewController.swift
//  weather_sberbank_task
//
//  Created by Иван Рыжухин on 23.06.2020.
//  Copyright © 2020 Ivan Ryzhukhin. All rights reserved.
//

import Foundation
import  UIKit

class WeatherViewController: UIViewController {

    // MARK:- Properties

    private let temp: Double
    private let weatherDescription: String

    private let tempLabel = UILabel()
    private let descriptionLabel = UILabel()

    // MARK: - Initialization

    init?(temp: Double?, description: String?) {
        guard let temp = temp, let description = description else {
            return nil
        }
        self.temp = temp
        self.weatherDescription = description
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {

        view.backgroundColor = .black

        addTemperatureLabel()
        addWeatherDescription()
    }

    private func addTemperatureLabel() {
        tempLabel.translatesAutoresizingMaskIntoConstraints = false
        tempLabel.font = UIFont.systemFont(ofSize: 24.0)
        tempLabel.text = "\(temp) °С"
        tempLabel.textColor = .white
        view.addSubview(tempLabel)
        tempLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        tempLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 32).isActive = true
        tempLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -32).isActive = true
    }

    private func addWeatherDescription() {
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.font = UIFont.systemFont(ofSize: 12)
        descriptionLabel.text = weatherDescription
        descriptionLabel.textColor = .white
        view.addSubview(descriptionLabel)
        descriptionLabel.topAnchor.constraint(equalTo: tempLabel.bottomAnchor, constant: 16).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        descriptionLabel.bottomAnchor.constraint(lessThanOrEqualTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 16).isActive = true
    }
}
