//
//  ViewController.swift
//  weather_sberbank_task
//
//  Created by Иван Рыжухин on 22.06.2020.
//  Copyright © 2020 Ivan Ryzhukhin. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    // MARK: - Properties
    private let service = Service()
    var city: String? {
        get {
            return textFeild.text
        }
        set{
            textFeild.text = newValue
        }
    }

    private let showWeatherButton = UIButton()
    private let textFeild = UITextField()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black

        addTextField()
        addShowWeatherButton()

    }

    private func addTextField() {
        textFeild.translatesAutoresizingMaskIntoConstraints = false
        textFeild.borderStyle = .roundedRect
        textFeild.layer.cornerRadius = 12
        view.addSubview(textFeild)

        textFeild.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        textFeild.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        textFeild.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
    }

    private func addShowWeatherButton() {
        showWeatherButton.setTitle("Показать погоду", for: .normal)
        showWeatherButton.addTarget(self, action: #selector(onShowWeatherPress), for: .touchUpInside)
        showWeatherButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(showWeatherButton)

        showWeatherButton.topAnchor.constraint(equalTo: textFeild.bottomAnchor, constant: 16).isActive = true
        showWeatherButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        showWeatherButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        showWeatherButton.bottomAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 16).isActive = true
    }

    @objc private func onShowWeatherPress () {
        guard let city = city?.trimmingCharacters(in: .whitespacesAndNewlines) else {
            return
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            
            self.service.getWeather(city: city) { (isSuccess, message, dataSource) in
                guard isSuccess, let dataSource = dataSource else {
                    self.showAlert(with: message)
                    return
                }

                let description: String? = dataSource.weather?.first?.description
                let temp: Double? = dataSource.main?.temp

                let vc = WeatherViewController(temp: temp, description: description)
                if let vc = vc {
                    self.navigationController?.pushViewController(vc, animated: true)
                }

            }
        }
    }

    private func showAlert (with message: String?) {
        let alert = UIAlertController.init(title: "Ошибка", message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ок", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }

}

